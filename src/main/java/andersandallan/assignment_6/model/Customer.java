package andersandallan.assignment_6.model;

// Model for customer.
public class Customer {

    private final String id;
    private final String name;
    private final String lastName;
    private final String country;
    private final String postalCode;
    private final String phoneNumber;
    private final String email;

    // Constructor
    public Customer(String id, String name, String lastName, String country, String postalCode, String phoneNumber, String email) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    // Getters
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getLastName() {
        return lastName;
    }
    public String getCountry() {
        return country;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public String getEmail() {
        return email;
    }

}
