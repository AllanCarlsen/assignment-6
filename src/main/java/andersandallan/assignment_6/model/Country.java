package andersandallan.assignment_6.model;

// Simple model for getting a country and its customer-count.
public class Country {
    private final String countryName;
    private final String customerCount;


    public Country(String countryName, String customerCount) {
        this.countryName = countryName;
        this.customerCount = customerCount;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCustomerCount() {
        return customerCount;
    }
}
