package andersandallan.assignment_6.model;

// Model for an invoice with a customer and its total.
public class Invoice {
    String id;
    String firstName;
    String total;

    public Invoice(String id, String firstName, String total) {
        this.id = id;
        this.firstName = firstName;
        this.total = total;
    }
}
