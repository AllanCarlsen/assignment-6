package andersandallan.assignment_6.dbConnectionManager;

import java.sql.Connection;
        import java.sql.SQLException;

public interface DatabaseConnectionFactory {
    Connection getConnection() throws SQLException;
}
