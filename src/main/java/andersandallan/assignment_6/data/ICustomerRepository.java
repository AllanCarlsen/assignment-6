package andersandallan.assignment_6.data;

import andersandallan.assignment_6.model.Country;
import andersandallan.assignment_6.model.Customer;
import andersandallan.assignment_6.model.Invoice;

import java.util.Collection;

public interface ICustomerRepository {
    Collection<Customer> getAllCustomers();
    Customer getCustomerById(int customerId);
    Customer getCustomerByName(String firstName, String lastName);
    Collection<Customer> getCustomerList(int limit, int offset);
    int addNewCustomer(Customer customer);
    int updateExistingCustomer (String customerId, Customer customer);
    Collection<Country> getCountriesByNumberOfCustomerOrderDescending();
    Collection<Invoice> getCustomersByHighestSpending();
}
