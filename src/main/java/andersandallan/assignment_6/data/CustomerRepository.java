package andersandallan.assignment_6.data;

import andersandallan.assignment_6.dbConnectionManager.DatabaseConnectionFactory;
import andersandallan.assignment_6.model.Country;
import andersandallan.assignment_6.model.Customer;
import andersandallan.assignment_6.model.Invoice;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;


@Repository
public class CustomerRepository implements ICustomerRepository{

    private final DatabaseConnectionFactory connectionFactory;

    // Constructor
    public CustomerRepository(DatabaseConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public Collection<Customer> getAllCustomers() {
        // Creates connection to database
        try (Connection conn = connectionFactory.getConnection()){
            // Preparing a SQL-query.
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Customer");
            // Executing the query and saving the result-set
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Customer> customers = new ArrayList<>();

            // While resultSet has a new line, it populates the customer model, ande saves it in an array of customers.
            while(resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );

                customers.add(customer);
            }
            // Returns array of customers.
            return customers;

        } catch (SQLException sqe) {
            // Prints stacktrace and returns null.
            sqe.printStackTrace();
            return null;
        }
    }

    public Customer getCustomerById(int customerId){
        try (Connection conn = connectionFactory.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Customer WHERE CustomerId = ?");
            preparedStatement.setInt(1, customerId);
            ResultSet resultSet = preparedStatement.executeQuery();

            return new Customer(
                    resultSet.getString("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    public Customer getCustomerByName(String firstName, String lastName){
        try (Connection conn = connectionFactory.getConnection()){
            // '?' are placeholders/variables that we set right beneath.
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Customer WHERE FirstName LIKE ? AND LastName LIKE ?");
            preparedStatement.setString(1, "%" + firstName + "%");
            preparedStatement.setString(2, "%" + lastName + "%");
            ResultSet resultSet = preparedStatement.executeQuery();

            return new Customer(
                    resultSet.getString("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    public Collection<Customer> getCustomerList(int limit, int offset) {
        try (Connection conn = connectionFactory.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM Customer LIMIT ? OFFSET ?");
            preparedStatement.setInt(1, limit);
            // to prevent skipping the first customer, we minus the offset with 1.
            preparedStatement.setInt(2, offset-1);
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Customer> customers = new ArrayList<>();

            while(resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );

                customers.add(customer);
            }
            return customers;

        } catch (SQLException sqe) {
            sqe.printStackTrace();
            return null;
        }
    }

    public int addNewCustomer(Customer customer) {
        int result = 0;
        try (Connection conn = connectionFactory.getConnection()) {
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "INSERT INTO Customer " +
                            "(CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email) " +
                            "VALUES (?,?,?,?,?,?,?) ");
            preparedStatement.setString(1, customer.getId());
            preparedStatement.setString(2, customer.getName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());

            // executeUpdate() is used for DML(Data Manipulation Language) statements, such as INSERT, UPDATE and DELETE.
            // This also returns an integer.
            result = preparedStatement.executeUpdate();

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return result;
    }

    public int updateExistingCustomer (String customerId, Customer customer) {
        int result = 0;
        try (Connection conn = connectionFactory.getConnection()) {

            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE Customer " +
                            "SET " +
                                "FirstName = ?," +
                                "LastName = ?," +
                                "Country = ? ," +
                                "PostalCode = ?," +
                                "Phone = ?," +
                                "Email = ? " +
                            "WHERE CustomerId = ?");
            preparedStatement.setString(1, customer.getName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setString(7, customerId);

            result = preparedStatement.executeUpdate();


        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return result;
    }

    public Collection<Country> getCountriesByNumberOfCustomerOrderDescending () {
        ResultSet resultSet;
        Country result;
        ArrayList<Country> resultList = null;
        try (Connection conn = connectionFactory.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Country, COUNT(CustomerId) FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC");
            resultSet = preparedStatement.executeQuery();

            resultList = new ArrayList<>();
            while(resultSet.next()) {
                result = new Country(
                        resultSet.getString("Country"),
                        resultSet.getString("count(customerId)"));

                resultList.add(result);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return  resultList;
    }

    // This will get a list of the highest amount spend on a single invoice, and not the total amount a customer has spent.
    public Collection<Invoice> getCustomersByHighestSpending() {
        ResultSet resultSet;
        Invoice result;
        ArrayList<Invoice> resultList = null;
        try (Connection conn = connectionFactory.getConnection()){
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT Customer.CustomerId, Customer.FirstName, Invoice.Total FROM Invoice " +
                    "JOIN Customer " +
                    "USING(customerid) " +
                    "ORDER BY total DESC");
            resultSet = preparedStatement.executeQuery();

            resultList = new ArrayList<>();
            while(resultSet.next()) {
                result = new Invoice(
                        resultSet.getString("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("Total"));

                resultList.add(result);
            }

        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return  resultList;
    }
}
