package andersandallan.assignment_6.controller;

import andersandallan.assignment_6.data.ICustomerRepository;
import andersandallan.assignment_6.model.Country;
import andersandallan.assignment_6.model.Customer;
import andersandallan.assignment_6.model.Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
// Provides all the endpoints to the application.
@RestController
@RequestMapping("api")
public class CustomerController {
    private final ICustomerRepository customerRepository;

    // Constructor
    public CustomerController(@Autowired ICustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @GetMapping("customers")
    public Collection<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("customer/{customersId}")
    public Customer getCustomerById(@PathVariable Integer customersId){
        return customerRepository.getCustomerById(customersId);
    }
    //http://localhost:8080/api/customer/search?firstName=Daan&lastName=Peeters
    @GetMapping("customer/search")
    public Customer getCustomerByName(@RequestParam String firstName,@RequestParam String lastName){
        return customerRepository.getCustomerByName(firstName, lastName);
    }
    //http://localhost:8080/api/customer/list?limit=10&offset=50
    @GetMapping("customer/list")
    public Collection<Customer> getCustomerList(@RequestParam int limit,@RequestParam int offset){
        return customerRepository.getCustomerList(limit, offset);
    }

    @PostMapping("customer/add")
    public int addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addNewCustomer(customer);
    }

    //localhost:8080/api/customer/60
    @PutMapping("customer/{customerId}")
    public int updateExistingCustomer(@PathVariable String customerId, @RequestBody Customer customer){
        return customerRepository.updateExistingCustomer(customerId, customer);
    }

    @GetMapping("countries")
    public Collection<Country> getCountriesByNumberOfCustomerOrderDescending(){
        return customerRepository.getCountriesByNumberOfCustomerOrderDescending();
    }

    @GetMapping("customer/totalSpendingDesc")
    public Collection<Invoice> getCustomersByHighestSpending(){
        return customerRepository.getCustomersByHighestSpending();
    }
}
