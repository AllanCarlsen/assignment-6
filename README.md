# Assignment 6 - Access and expose a database

We are developing an app to access and interact with the Chinook database (see usage below).
We have been pair-programming through IntelliJ's own tool, and took turns at sitting behind the wheel and beeing the navigator.

## Author
Anders Madsen &
Allan Carlsen

## Installation

The app can be run in IntelliJ and supposedly other IDE's for JAVA as well.
Data is retrieved via a browser.
To add to or edit the tables of the database, you can use eg. the Postman App.

## Usage

1. To read all the customers in the database, open a browser and enter into the adress line
   localhost:8080/api/customers
   This displays their: Id, first name, last name, country, postal code, phone number and email as a JSON object.

2. To read a specific customer from the database (by Id), enter
   localhost:8080/api/customer/x
   where x is the id number of the customer.
   This displays everything listed in the above point for that customer as a JSON object.

3. To read a specific customer by name, enter (to get the customer Daan Peeters as JSON object)
   localhost:8080/api/customer/search?firstName=Daan&lastName=Peeters
   Partial names will suffice, but there must be both a first and a last name of at least one letter.

4. To get a page of customers (as JSON object) from the database, enter
   http://localhost:8080/api/customer/list?limit=LIMIT&offset=OFFSET
   where LIMIT is replaced by the number of customers to be retrieved and OFFSET by the customer Id of first customer to be retrieved (counting as per usual from 1 and up).

5. To add a new customer to the database with the fields listed above (in a customer object), use eg. Postman to make an HTTP post request to
   localhost:8080/api/customer/add
   with the corresponding JSON object as data body in raw format.
   Eg.
   {
   "id": "60",
   "name": "Holger",
   "lastName": "Danske",
   "country": "Denmark",
   "postalCode": "2200",
   "phoneNumber": "+45 12345678",
   "email": "hd@gmail.com"
   }


6. To update an existing customer, use eg. Postman to make a put request to
   localhost:8081/api/customer/update
   with corresponding JSON object as data body in raw format.

7. To return the number of customers in each country, ordered descending, as a JSON object, enter into the address line of a browser
   localhost:8080/api/countries

8. To get a list (as JSON) of customers who are the highest spenders (total in invoice table is the largest), ordered descending, enter into the browser
   localhost:8080/api/customer/totalSpendingDesc
   This returns every invoice in the database with the corresponding name of the customer who made the purchase.
